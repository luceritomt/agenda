<?php


namespace Modelos;


class agenda extends conexio
{
    public $id;
    public $Nombre;
    public $Apellido;
    public  $Telefono;
    //llenado de cada campo
    public function llanadocam(){
        if(isset($_POST['Nombre'])){
            $this->Nombre=$_POST['Nombre'];
        }
        if ((isset($_POST['apellido']))){
            $this->Apellido=$_POST['Apellido'];
        }
        if(isset($_POST['Telefono'])){
            $this->Telefono=$_POST['Telefono'];
        }
    }
    //consultar contactos por nombre
    public function  consultar($Telefono){
        $conexio=new conexio();
        $pre=mysqli_prepare($conexio->connec,"SELECT* FROM contactos");
        $pre->execute();
        $re=$pre->get_result();
        while ($y=mysqli_fetch_assoc($re)){
            $t[]=$y;
        }
        return $t;
    }
    //funcion para crear contactos
    public function  crear(){
        $this->llanadocam();
        $pre=mysqli_prepare($this->connec,"INSERT INTO contactos (Nombre,Apellido,Telefono)VALUES (?,?,?,)");
        $pre->bind_param("sss",$this->Nombre,$this->Apellido,$this->Telefono);
        $pre->execute();
    }
    //funcion para editar contactos
    public function editar(){
        $pre=mysqli_prepare($this->connec,"UPDATE contactos SET Nombre=?,Apellido=?,Telefono=? where id=?");
        $pre->bind_param("isss",$this->id,$this->Nombre,$this->Apellido,$this->Telefono);
        $pre->execute();
    }
    //funcion para eliminar un contacto
    public static function elimar($dato){
        $conexio=new conexio();
        $pre=mysqli_prepare($conexio->connec,"DELETE FROM contactos WHERE id=?");
        $pre->bind_param("s",$dato);
        $pre->execute();
    }
}
?>